# MIRAGE Ontologies

This repository will provide the list of vocabulary used for the MIRAGE-supported repositories
GlycoPOST and UniCarb-DR.  This list will be provided as an OWL file as well as a hierarchically organized
list of identifiers for each term.
